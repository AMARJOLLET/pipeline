package pipeline;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WelcomePage extends AbstractPage {
    public WelcomePage(WebDriver driver) {
        super(driver);
    }
    

    @FindBy(xpath = "//a[text()='Enter the Store']")
    private WebElement enterStoreButton;


    public ShopPage clickButtonEnterStore(WebDriverWait wait){
        seleniumTools.clickOnElement(wait, enterStoreButton);
        return new ShopPage(driver);
    }
}
