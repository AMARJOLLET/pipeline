package pipeline;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import utils.SeleniumTools;

public abstract class AbstractPage {
    protected static final Logger LOGGER = LogManager.getLogger();
    protected WebDriver driver;

    protected final SeleniumTools seleniumTools = new SeleniumTools();

    /**
     * Constructeur extends sur tous les pages objets pour mutualisé la page factory
     * Possibilité de mettre le driver en static et éviter de le mettre dans le constructeur
     * 
     * @param driver le webdriver utilisé dans le test
     */
    protected AbstractPage(WebDriver driver){
        this.driver = driver;
        SeleniumTools.myPageFactory(driver, AbstractPage.this);
    }



}
