package pipeline;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ConnectionPage extends AbstractPage {

    public ConnectionPage(WebDriver driver) {
        super(driver);
    }
    

    @FindBy(name = "username")
    private WebElement userField;

    @FindBy(name = "password")
    private WebElement passwordField;

    @FindBy(name = "signon")
    private WebElement loginButton;


    public void fillUsername(WebDriverWait wait, String username){
        seleniumTools.sendKey(wait, userField, username);
    }

    public void fillPassword(WebDriverWait wait, String pasword){
        seleniumTools.sendKey(wait, passwordField, pasword);
    }

    public ShopPage clickSignIn(WebDriverWait wait){
        seleniumTools.clickOnElement(wait, loginButton);
        return new ShopPage(driver);
    }

}
