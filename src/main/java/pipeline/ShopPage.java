package pipeline;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ShopPage extends AbstractPage {
    public ShopPage(WebDriver driver) {
        super(driver);
    }
    
    @FindBy(xpath = "//a[contains(@href, 'signon')]")
    private WebElement signInButton;

    @FindBy(id = "WelcomeContent")
    private WebElement connectedText;


    public ConnectionPage clickSignInButton(WebDriverWait wait){
        seleniumTools.clickOnElement(wait, signInButton);
        return new ConnectionPage(driver);
    }

    public String getConnectedText(WebDriverWait wait){
        return wait.until(ExpectedConditions.visibilityOf(connectedText)).getText();
    }
}
