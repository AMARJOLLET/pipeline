package utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumTools {
    private static final List<Object> pageObjectList = new ArrayList<>();
    private static final Pattern PATTERN_LINE_BREAK = Pattern.compile("[\\n\\r]");
    private static final Logger LOGGER = LogManager.getLogger(SeleniumTools.class);


    public void clickOnElement(WebDriverWait wait, WebElement we){
        wait.until(ExpectedConditions.elementToBeClickable(we));
        String weName = getWeName(we, nameWebElementLogging(we));
        we.click();
        LOGGER.info("Click on element : {}", weName);
    }


    public void sendKey(WebDriverWait wait, WebElement we, String str){
        wait.until(ExpectedConditions.elementToBeClickable(we));
        String weName = getWeName(we, nameWebElementLogging(we));
        we.clear();
        we.sendKeys(str);
        LOGGER.info("Sendkey on element : {} with {}", weName, str);
    }

    /*
     * Les méthodes ci-dessous permettent juste de faire un message de log. 
     * On regarde si on utilise un @FindBy :
     * Si oui, alors on affiche le nom de la variable du webelement au niveau du code java.
     * Si non, alors on affiche le text du webelement / attribut
     */

    /**
     * Récupère l'objet portant les noms des variables
     * @param driver WebDriver utilisé dans le test
     * @param pageObject l'objet portant les @FindBy que l'on veut peupler
     */
    public static void myPageFactory(WebDriver driver, Object pageObject){
        PageFactory.initElements(driver, pageObject);
        pageObjectList.add(pageObject);
    }

    /**
     * Récupère le nom de la variable du code java
     * @param webElement le webelement a intéragir
     * @param anotherLogMessage le nom a affiché dans les logs si il n'y a pas de match avec une variable du code java (ex: un Webelement défini avec un driver.findElement)
     * @return le nom a affiché dans les logs quand on fait une action avec le webElement
     */
    private static String getWeName(WebElement webElement, String anotherLogMessage) {
        String webElementName = anotherLogMessage;
        if (!pageObjectList.isEmpty()) {
            var fieldNameList = pageObjectList.stream()
                    .map((Object obj) -> Arrays.stream(obj.getClass().getDeclaredFields())
                            .filter((Field field) -> {
                                try {
                                    boolean isAccess = field.canAccess(obj);
                                    field.setAccessible(true);
                                    boolean isField = field.get(obj) == webElement;
                                    field.setAccessible(isAccess);
                                    return isField;
                                } catch (IllegalAccessException | IllegalArgumentException e) {
                                    return false;
                                }
                            }).collect(Collectors.toList())
                    )
                    .flatMap(List::stream)
                    .map(Field::getName)
                    .collect(Collectors.toList());

            if (fieldNameList.isEmpty()) {
                LOGGER.debug("FieldNameList empty, classList size : {}", pageObjectList.size());
            } else if (fieldNameList.size() > 1) {
                LOGGER.info("FieldNameList size : {}\n{}", fieldNameList.size(), fieldNameList);
            } else {
                webElementName = fieldNameList.get(0);
            }
        } else {
            LOGGER.debug("classList is null");
        }
        return webElementName;
    }

    /**
     * Formatage du nom du webElement car si on a un select, ca fait un nom un peu bizarre
     * @param webElement WebElement a intéragir
     * @return nom du webelement pour les logs
     */
    public String nameWebElementLogging(WebElement webElement) {
        String webElementName = getWebElementName(webElement);
        return PATTERN_LINE_BREAK.matcher(webElementName).replaceAll(", ").trim();
    }

    /**
     * Permet de générer le nom du webElement a affiché dans les logs quand il n'y a pas de match avec la méthode getWeName
     * @param element webelement a intéragir
     * @return nom a affiché par défaut
     */
    private static String getWebElementName(WebElement element) {
        String webElementName = element.getText();
        if (webElementName != null && !webElementName.isEmpty()) {
            return webElementName;
        }

        List<String> attributNameList = List.of("name", "title", "id", "value");
        for (String attributName : attributNameList) {
            webElementName = element.getAttribute(attributName);
            if (webElementName != null && !webElementName.isEmpty()) {
                return webElementName;
            }
        }
        return String.format("WebElement : %s", element);
    }


}
