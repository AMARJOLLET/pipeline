package selenium_test;

import java.time.Duration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.chromium.ChromiumOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.AbstractDriverOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * L'annotation @ExtendWith permet de rajouter des règles de lancement pour junit jupiter. La classe TestResult implémente 2 interfaces Junit jupiter. Sans l'annotation, le lancement ne tiend pas en compte de la classe TestResult
 */
@ExtendWith(TestResult.class)
public abstract class AbstractTest {
    protected final Logger logger = LogManager.getLogger(AbstractTest.this);
    protected static WebDriver driver;
    protected WebDriverWait wait;

    private final Duration implicitWait = Duration.ofSeconds(1);
    private final Duration explicitWait = Duration.ofSeconds(3);
    
    /**
     * Le System.getProperty permet de récupérer une variable que l'on crée avec une commande maven
     * Ex : mvn clean test -Dbrowser=chrome => on lancera avec chrome
     */
    private final String browser = System.getProperty("browser", "firefox");

    /**
     * Setup du driver en fonction du navigateur. On crée un enum car il n'existe que 3 navigateurs (on peut rajouter si on veut IE)
     */
    @BeforeEach
    void setup() {
        logger.info("Setup driver with browser : {}", browser);
        System.out.println(System.getProperty("os.name"));
        switch (Browser.valueOf(browser.toUpperCase())) {
            case CHROME:
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--remote-allow-origins=*");
                setupHeadless(chromeOptions);
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver(chromeOptions);
                break;
            case FIREFOX:
                WebDriverManager.firefoxdriver().setup();
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                if (System.getProperty("os.name").equalsIgnoreCase("linux")){
                    firefoxOptions.addArguments("--headless");
                }
                setupHeadless(firefoxOptions);
                driver = new FirefoxDriver(firefoxOptions);
                break;
            case EDGE:
                EdgeOptions edgeOptions = new EdgeOptions();
                edgeOptions.addArguments("--remote-allow-origins=*");
                setupHeadless(edgeOptions);
                WebDriverManager.edgedriver().setup();
                driver = new EdgeDriver(edgeOptions);
                break;
        }

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(implicitWait);
        wait = new WebDriverWait(driver, explicitWait);
    }

    public <T extends AbstractDriverOptions<?>> void setupHeadless(T options){
        if (System.getProperty("os.name").equalsIgnoreCase("linux")){
            if (options instanceof ChromiumOptions){
                ((ChromiumOptions<?>) options).addArguments("--headless=new");
                ((ChromiumOptions<?>) options).addArguments("--disable-gpu");
                ((ChromiumOptions<?>) options).addArguments("--no-sandbox");
            } else if (options instanceof FirefoxOptions){
                ((FirefoxOptions) options).addArguments("--headless");
            } else {
                logger.error("Incorrect driver option");
            }
        }
    }

}
