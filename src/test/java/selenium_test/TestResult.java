package selenium_test;


import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class TestResult implements TestWatcher, BeforeAllCallback {
    private static Logger LOGGER; 
    private static String CREATION_DATE;
    private static String dataFilePath;

    /**
     * Le beforeAllCallBack permet de faire des actions avant le lancement des tests. Très utile pour setup un logger
     * Ici ThreadContext permet de surcharger la valeur du log4j2.xml afin d'avoir un dossier dédié pour notre test
     */
    @Override
    public void beforeAll(ExtensionContext context) throws Exception {
        CREATION_DATE = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy_HH.mm.ss"));
        dataFilePath = String.format("target/data/test_%s", CREATION_DATE);
        ThreadContext.clearAll();
        ThreadContext.put("logFile", String.format("%s/log_test.log", dataFilePath));
        LOGGER = LogManager.getLogger(); 
    }

    /**
     * Ci-dessous se trouve les 3 méthodes du TestWatcher : testSuccessfull, testAborted et testFailed
     * Le testAborted ne correspond une interruption brutal avec un code exit system (bouton arret ou ctr+C d'un build maven)
     * Une utilité très intéressant de cette interface est : ExtensionContext context
     * Cette ExtensionContext permet de récupèrer le nom de la classe de test lancé, les méthodes .etc. 
     */

    @Override
    public void testSuccessful(ExtensionContext context) {
        LOGGER.info("Test successful !!");
        AbstractTest.driver.quit();
    }

    @Override
    public void testAborted(ExtensionContext context, Throwable cause) {
        LOGGER.error("Test aborted with cause : \n{}", ExceptionUtils.getStackTrace(cause));
        if (AbstractTest.driver != null){
            // context.getTestClass().orElse(null).getSimpleName() permet de récupèrer le nom de la classe de test si elle existe. getSimpleName = nom de la classe sans les packages
            takeSnapshot(context.getTestClass().orElse(null).getSimpleName());  
            AbstractTest.driver.quit();
            AbstractTest.driver = null;
        }
    }

    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        LOGGER.error("Test failed with cause : \n{}", ExceptionUtils.getStackTrace(cause));
        if (AbstractTest.driver != null){
            // context.getTestClass().orElse(null).getSimpleName() permet de récupèrer le nom de la classe de test si elle existe. getSimpleName = nom de la classe sans les packages
            takeSnapshot(context.getTestClass().orElse(null).getSimpleName());
            AbstractTest.driver.quit();
            AbstractTest.driver = null;
        }
    }

    /**
     * Méthode classique pour prendre un snapshot. On défini le dossier pour le snapshot. C'est le même que celui des logs pour centraliser les éléments du test.
     * @param className nom de la classe du test
     */
    public void takeSnapshot(String className){
        var snapFilePath = String.format("%s/snapshot_%s.png", dataFilePath, CREATION_DATE);
        LOGGER.info("---------- Snapshot available in {} ----------", snapFilePath);
        TakesScreenshot scrShot = ((TakesScreenshot) AbstractTest.driver);
        var srcFile = scrShot.getScreenshotAs(OutputType.FILE);
        var destFile = new File(snapFilePath);
        try {
            FileUtils.copyFile(srcFile, destFile);
        } catch (IOException e) {
            LOGGER.error("Can't copy snapshot in {}", destFile.getName());
            LOGGER.error(ExceptionUtils.getStackTrace(e));
        }

    }

}
