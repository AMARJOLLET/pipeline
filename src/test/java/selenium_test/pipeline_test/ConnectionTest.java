package selenium_test.pipeline_test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.junit.jupiter.api.Test;

import pipeline.ShopPage;
import pipeline.ConnectionPage;
import pipeline.WelcomePage;
import selenium_test.AbstractTest;

class ConnectionTest extends AbstractTest {
    private final static Properties properties = new Properties();

    /**
     * Permet de peupler la variable properties -> On modifiera le fichier .properties pour avoir d'autres JDD
     */
    static {
        try {
            InputStream fileAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(String.format("data_test/%s.properties", ConnectionTest.class.getSimpleName()));
            properties.load(fileAsStream);
        } catch (IOException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    private final String BROWSER_URL = properties.getProperty("url");
    private final String USERNAME = properties.getProperty("username");
    private final String PASS = properties.getProperty("password");

    @Test
    void testConnection() {
        logger.info("Start of the Connection's Test");
        logger.info("Get url {}", BROWSER_URL);
        driver.get(BROWSER_URL);

        logger.info("Access Welcome Page");
        WelcomePage welcomePage = new WelcomePage(driver);
        ShopPage shopPage = welcomePage.clickButtonEnterStore(wait);

        logger.info("Access Shop page");
        ConnectionPage connectionPage = shopPage.clickSignInButton(wait);

        logger.info("Access Connection page");
        connectionPage.fillUsername(wait, USERNAME);
        connectionPage.fillPassword(wait, PASS);

        logger.info("Connected as {}", USERNAME);
        shopPage = connectionPage.clickSignIn(wait);
        assertEquals("Welcome ABC!", shopPage.getConnectedText(wait));

        logger.info("Connection's Test done");
    }
}
